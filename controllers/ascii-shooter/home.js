//var Video = require('../models/user');

var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {//home
	var scriptDir = '/javascripts/ascii-shooter/';

	res.render('ascii-shooter/home', {
		title: 'ASCII Shooter',
		scripts: [
			'//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js',
			'/socket.io/socket.io.js',
			'/javascripts/howler/howler.min.js',
			'EventEmitter.js',
			'View.js',
			'ModelClient.js',
			'WebView.js',
			'WebController.js',
			scriptDir + 'init.js'
		]
	});
});

module.exports = router;
