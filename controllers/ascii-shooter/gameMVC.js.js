//var Video = require('../models/user');

var fs      = require('fs');
var path    = require('path');
var express = require('express');
var router  = express.Router();

/* GET home page. */

serveStatic(router, 'modelIO/ModelClient.js', 'ModelClient.js');
serveStatic(router, 'View.js');
serveStatic(router, 'web/WebView.js', 'WebView.js');
serveStatic(router, 'web/WebController.js', 'WebController.js');
serveStatic(router, 'common/EventEmitter.js', 'EventEmitter.js');

module.exports = router;


function serveStatic(router, file, url) {
	url = url || file;

	router.get('/' + url, function(req, res, next) {
		res.send(fs.readFileSync(__dirname + '/../../models/ascii-shooter/' + file));
	});
}
