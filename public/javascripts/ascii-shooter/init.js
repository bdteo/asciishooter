$(document).ready(function ready() {
	var _model      = new ModelClient();

	var _view       = new WebView(_model);
	var _controller = new WebController(_model);
	
	_model.init();
	_view.init();
	_controller.init();
})

