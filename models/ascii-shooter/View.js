var GameView;

(function () {
	
	/* construct */
	var rawHeroTemplate = [
				' - ',
				'-?-',
				' - '
			],
			rawHeroTemplateLen = Math.floor(rawHeroTemplate.length/2);

	var heroesCompiled,
			bullets;
	var map;
	var model;

	/**
	 *	@throws if _model argument not valid (width, height)
	 */
	GameView = function GameView(_model) {
		if (!_model || isNaN(_model.height) || isNaN(_model.width))
			throw 'Valid width and height needed';
		
		/* extend */
		EventEmitter.call(this);	
		
		/* construct */
		var that = this;
		model = _model;
		
		initializeMap();
		drawWalls();
		
		var lastUsedXYs = {
			heroes  : [],
			bullets : []
		};
	
		var sounds = {
			gun          : new Howl({urls: ['/sounds/gunSilent.wav'] }),
			bulletGone   : new Howl({urls: ['/sounds/bulletGone.wav'] }),
			death        : new Howl({urls: ['/sounds/suffer.wav'] }),
			suffer       : new Howl({urls: ['/sounds/bodyHit2.wav'] }),
			music        : new Howl({urls: ['/sounds/music.wav'], autoplay: true, loop: true, volume: 0.25 })			
		};
		
		model.on('heroesChange', function (data) { updateHeroes(data) });
		model.on('heroesMove'  , moveHeroes);
		model.on('heroesDied', function (heroes) {		
			summarizeDeaths(heroes);
			sounds.death.play();
		});
		model.on('heroHealthChanged', function () { sounds.suffer.play() });
				
		model.on('bulletsMove', moveBullets);
		model.on('bulletFired', function () { sounds.gun.play() });
		
		setInterval(function () {
			render();
		}, 1000/30);//up to 30 frames per second
		
		/* define */
		function render() {
			var result = '';
			for (var i = 0, il = map.length; i<il; ++i)
				result += map[i].join('') + '\n';

			that.emit('render', result);
		}

		function moveHeroes(heroes) {
			clearXYs(lastUsedXYs.heroes);
			lastUsedXYs.heroes = [];

			for (var id in heroes)
				moveHero(id, heroes[id]);
			
			//render();
		}
		
		function moveBullets(bullets) {
			clearXYs(lastUsedXYs.bullets);
			lastUsedXYs.bullets = [];
			
			var b;
			
			for (var id in bullets) {
				b = bullets[id];
				draw(b.x, b.y, '*');
				lastUsedXYs.bullets.push(b);
			}

			//render();
		}
		
		
		function moveHero(id, hero) {
			if (!hero.health)
				return;

			var H = heroesCompiled[id],
					x, y;

			for (var j = 0, jl = H.length; j<jl; ++j) {
				x = hero.XY.x + H[j].x - rawHeroTemplateLen;
				y = hero.XY.y + H[j].y - rawHeroTemplateLen;

				draw(x, y, H[j].char);

				lastUsedXYs.heroes.push({
					x: x,
					y: y
				});
			}
		}

		function updateHeroes(heroes) {
			summarizeDeaths(heroes);
			
			heroesCompiled = {};

			for (var id in heroes) {
				heroesCompiled[id] = new HeroSprite(heroes[id]).make();
			}
				
			moveHeroes(heroes);
		}

		function clearXYs(XYs) {
			if (!XYs)
				return;

			for (var i = 0, il = XYs.length; i<il; ++i) {			
				draw(XYs[i].x, XYs[i].y, ' ');
			}
		}

		function draw(x, y, char) {
			if (map[y] === undefined)
				return;
			if (map[y][x] === undefined)
				return;

			map[y][x] = char;
		}

		function initializeMap() {
			map = [];

			for (var i = 0; i<model.height; ++i) {
				var row = [];
				for (var j = 0; j<model.width; ++j)
					row.push(' ');
				map.push(row);
			}
		}
		
		function drawWalls() {
			var mapFile = model.mapFile;
			
			for (var i = 0; i<model.height; ++i) {
				var line = mapFile[i];

				for (var j = 0; j<model.width; ++j) {
					var char = line.charAt(j);
					if (char !== ' ')
						draw(j, i, '#');
				}
			}
		}

		function HeroSprite(state) {
			var result;

			this.make = function make() {
				result = [];

				for (var y = 0, yl = rawHeroTemplate.length; y<yl; ++y) {
					var line = rawHeroTemplate[y];//rawHeroTemplate[y].split('');

					for (var x = 0, xl = line.length; x<xl; ++x) {
						var chr = line.charAt(x);
						if (chr !== ' ') {
							if (chr === '-')//single digit or character to make sprites distinguishable
								chr = idToChar(state.id);
							else if (chr === '?')
								chr = state.health;
							
							result.push({
								x    : x,
								y    : y,
								char : chr
							});
						}
					}
				}

				return result.slice();
			};
		}

		
		function idToChar(id) {
			return String.fromCharCode(65 + (id % 26))
		}
		
		function summarizeDeaths(heroes) {
			var deathsSummary = [];
			for (var id in heroes) { deathsSummary.push(heroes[id]) }			
			deathsSummary.sort(function sortByDeaths(heroA, heroB) { return heroA.deaths - heroB.deaths });

			deathsSummary = deathsSummary.map(function (hero) { return hero.nickname + '(' + idToChar(hero.id) + ')-' + hero.deaths });
			that.emit('deaths', deathsSummary);
		}
	};	
})();


