var WebController;

(function () {
	var that = this;
	var model;

	WebController = function WebController(_model) {
		model = _model;
	}

	WebController.prototype.init = function init() {
		handleInGameEvents();
		handleHallEvents();
	}
	
	function handleHallEvents() {
		var commandInput = $('#commandInput').focus();
		
		$(document).click(function () {
			if (model.inGame())
				return;
				
			commandInput.focus();
		});
		
		$(document).keydown(function (e) {
			if (model.inGame())
				return;
			
			if (e.which === 13) {
				handleCommandInput();
				e.preventDefault();
			}
		});
		
		
		function handleCommandInput() {
			model.input(commandInput.val());
			commandInput.val('');
		}
	}
	
	
	function handleInGameEvents() {
		var chatInput = $('#chatInput');
		var gameField = $('#shoot-field');

		$(document).keydown(function (e) {
			if (!model.inGame())
				return;
			
			if (e.which === 13) {
				handleChatInput();
				e.preventDefault();
			}

			ifActionEnabled(function () {
				switch(e.which) {
					case 37: model.left(); break;
					case 38: model.up(); break;
					case 39: model.right(); break;
					case 40: model.down(); break;

					default:
						return;
				}

				e.preventDefault();
			});
		});

		$(document).keyup(function (e) {
			if (!model.inGame())
				return;
			
			ifActionEnabled(function () {
				switch(e.which) {
					case 65: model.shoot('left'); break;
					case 87: model.shoot('up'); break;
					case 68: model.shoot('right'); break;
					case 83: model.shoot('down'); break;

					case 37: model.stopX(); break;
					case 38: model.stopY(); break;
					case 39: model.stopX(); break;
					case 40: model.stopY(); break;

					default:
						return;
				}

				e.preventDefault();
			});
		});

		function ifActionEnabled(callback) {
			if (chatInput.is(":focus"))
				return;
			callback && callback();
		}
		
		function handleChatInput() {
			if (chatInput.is(":focus")) {
				if (chatInput.val()) {
					model.chat(chatInput.val());
					chatInput.val('');
				}
				else
					gameField.focus();
			}
			else {
				chatInput.focus();
			}
		}
	}
})();
