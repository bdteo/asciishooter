var WebView;

(function (exports) {	
	var model;
	var sounds = {
		notification : new Howl({urls: ['/sounds/notification.wav'], volume: 2 }),
	};

	WebView = function WebView(_model) {
		model = _model;		
	};
	
	
	WebView.prototype.init = function init() {
		var body = $('body');
		
		var field        = $('#shoot-field'),
				status       = $('#status'),
				chatMessages = $('#chatMessages'),
				commandInput = $('#commandInput'),
				hallOutput   = $('#hallOutput'),
				prompt       = $('#prompt'),

				gameElement  = $('#game-container');
				menuElement  = $('#menu-container');
		
		/* gamemodel events */
		model.on('render', function render(text) {
			field[0].innerHTML = text;
		});
		
		model.on('deaths', function deaths(deaths) {
			status[0].innerHTML = 'Deaths : ' + deaths.join(', ');
		});
		
		model.on('chatMsg', function chatMsg(text) {
			sounds.notification.play();
			var msg = $('<div>').text(text);
			chatMessages.prepend(msg);			
		});
		
		/* other events */		
		model.on('output', function output(line) {
			var text = document.createTextNode(line + '\n');

			hallOutput.append(text);
			body.scrollTop(commandInput.offset().top)
		});
		
		model.on('logged', function logged(nick) {
			prompt.text(nick + '>>');
		});
		
		model.on('gameStarted', function () {
			menuElement.hide(); gameElement.show()
		});
	};
})();
