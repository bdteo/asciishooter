var EventEmitter = require(__dirname + '/common/EventEmitter').EventEmitter;

var User = require(__dirname + '/gameAbstraction/User');

module.exports = function Hall() {
	/* extend */
	EventEmitter.call(this);
	
	/* construct */
	var that     = this;
	
	var usersByID       = {},
			usersByNickname = {};
	
	var rooms = {};
	
	/* define */
	/* getters */
	
	this.getUser = function getUser(userID) { return usersByID[userID] };
	this.getRoom = function getRoom(name)   { return rooms[name] };
	
	this.getUsersByNickName = function getUsersByNickName() { return usersByNickname };
	
	/* user managment methods */
	this.createUser = function createUser(userID) {
		if (usersByID[userID])
			return;//todo: user exists message

		var user = usersByID[userID] = new User(userID, that);		
		
		return user;	
	};
	
	this.removeUser = function removeUser(userID) {
		var nickname = usersByID[userID].nickname();
		
		delete usersByID[userID];
		if (nickname)
			delete usersByNickname[nickname];
	};
	
	
	/* room managment methods */
	this.enlistRoom = function enlistRoom(room) {
		var prevName = room.getPreviousName();
		var name     = room.getName();

		rooms[name] = room;
		
		delete rooms[prevName];
		
		//todo emit room added/deleted
	};
	
	this.removeRoom = function(room) {
		delete rooms[room.getName() || room.getPreviousName()];
	}
	
	
	this.listRooms = function listRooms() {
		var result = [];

		for (var name in rooms) {
			var room = rooms[name];
			
			result.push(name + '\n\t' + listRoom(room));
		}
		
		return result.join('\n');
	}
	
	function listRoom(room) {
		return room.getApprovedNames().join('\n\t');
	}

	/* helpers */
};
