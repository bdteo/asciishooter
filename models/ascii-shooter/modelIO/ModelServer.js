var Game = require(__dirname  + '/../gameAbstraction/Game');
var Hall  = require(__dirname  + '/../Hall');

module.exports = function ModelServer(app) {
	var io = app._io;	
	var hall = new Hall();

	var _sockets      = {},
			_socketsCount = 0;

	io.on('connection', function onConnection(socket) {
		_sockets[socket.id] = socket;
		++_socketsCount;
		
		
		var user = hall.createUser(socket.id);
		
		/* socket.on */
		['left', 'right', 'up', 'down', 'stopX', 'stopY', 'shoot', 'chat'].forEach(triggerGameAction);		
		['command', 'login'].forEach(triggerUserMethod);
		
		/* user.on */		
		user.on('createGame', function () {
			var room = user.getRoom(); if (!room) return;
			var game = room.getGame(); if (!game) return;

			['heroBorn', 'heroDead', 'heroLeft', 'heroHealthChange', 'heroMove', 'bulletFired', 'bulletMove', 'bulletGone', 'chatMsg'].forEach(function (eventName) {
				propagateGameEvents(eventName, room, game);
			});			
		});

		user.on('emitSelf', function emitSelf(data) { socket.emit(data.eventName, data.payload) })
		user.on('join', function (roomName) { socket.join(roomName) });

		socket.on('disconnect', function disconnect() {
			user.disconnect();
			delete _sockets[socket.id];
			--_socketsCount;
		});
		
		
		user.init();

		function propagateGameEvents(eventName, room, game) {
			game.on(eventName, function (data) {
				io.sockets.in(room.getName()).emit(eventName, data);
			});
		}

		function triggerUserMethod(eventName) {
			socket.on(eventName, function (data) { if (!user[eventName]) return; user[eventName](data) });
		}
		
		function triggerGameAction(action) {
			socket.on(action, function (data) { user.play(action, data) });
		}
	});
};


var timedout = [];
function delay(callback, broadcast) {
	callback();
	/*if (broadcast)
		setTimeout(callback, 100);//timedout.push(callback);
	else
		setTimeout(callback, 90);*/
}

setInterval(function () { if (timedout.length > 0) timedout.shift()(); }, 100);
