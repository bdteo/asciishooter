var ModelClient;

(function () {
	var loggedIn = false;
	var nick     = '';

	var heroes  = {};
	var bullets = {};

	var socket;

	ModelClient = function ModelClient() {
		/* extend */
		EventEmitter.call(this);

		/* construct */
		var that = this;

		var _inGame = false;

		/* define */
		this.init = function init() {
			socket = io.connect();

			socket.on('requireLogin', function (line) { that.emit('output', line) });
			socket.on('output', function (line) { that.emit('output', line) });
			socket.on('logged', function (nickname) { nick = nickname; loggedIn = true; that.emit('logged', nickname) });

			socket.on('gameStarted', function (data) {
				_inGame = true;
				
				that.mapFile = data.mapFile;
				that.width   = data.mapFile[0].length;
				that.height  = data.mapFile.length;
				
				onGameStarted(data);
				var gameView = new GameView(that);
				gameView.onAll(function (data, eventName) {
					that.emit(eventName, data);
				});
			});			
		}

		this.input = function input(line) {
			that.emit('output', nick + '>>' + line);
			if (loggedIn) { socket.emit('command', line) }
			else { socket.emit('login', line) }
		}

		this.inGame = function inGame() {
			return _inGame;
		}

		function onGameStarted(data) {
			
			
			that.emit('gameStarted');
			
			socket.on('heroBorn', function (hero) { heroes[hero.id] = hero; that.emit('heroesChange', heroes) });
			socket.on('heroDead', function heroDied(hero) {
				if (!heroes[hero.id])
					return;

				heroes[hero.id] = hero;
				that.emit('heroesDied', heroes);
				that.emit('heroesMove', heroes);
			});
			
			socket.on('heroLeft', function heroLeft(hero) {
				if (!heroes[hero.id])
					return;

				delete heroes[hero.id];
				that.emit('heroesDied', heroes);
				that.emit('heroesMove', heroes);
			});

			socket.on('heroHealthChange', function (hero) {
				if (!heroes[hero.id]) return;

				that.emit('heroHealthChanged');

				heroes[hero.id].health = hero.health;
				that.emit('heroesChange', heroes);
			});
			socket.on('heroMove', function (hero) {
				if (!heroes[hero.id]) return;
				heroes[hero.id].XY = hero.XY;
				that.emit('heroesChange', heroes);
			});

			socket.on('bulletFired', function (bullet) { bullets[bullet.id] = bullet; that.emit('bulletFired') });
			socket.on('bulletMove', function bulletMove(bullet) { if (bullets[bullet.id]) bullets[bullet.id] = bullet; that.emit('bulletsMove', bullets) });
			socket.on('bulletGone', function bulletGone(bullet) { that.emit('bulletGone', bullets); delete bullets[bullet.id]; that.emit('bulletsMove', bullets) });

			
			socket.on('chatMsg', function (hero) { that.emit('chatMsg', heroes[hero.id].nickname + ': ' + hero.text) });
		}
	};


	ModelClient.prototype.up    = function () { socket.emit('up') };
	ModelClient.prototype.down  = function () { socket.emit('down') };
	ModelClient.prototype.left  = function () { socket.emit('left') };
	ModelClient.prototype.right = function () { socket.emit('right') };

	ModelClient.prototype.stopX = function () { socket.emit('stopX') };
	ModelClient.prototype.stopY = function () { socket.emit('stopY') };

	ModelClient.prototype.shoot = function (direction) { socket.emit('shoot', direction) };

	ModelClient.prototype.chat  = function (text) { socket.emit('chat', text) };
})();
