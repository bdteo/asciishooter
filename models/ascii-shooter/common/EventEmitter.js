(function (exports) {
	var EventEmitter = function EventEmitter() {
		var events = {};		
		
		this.on     = on;
		this.onAll  = onAll;
		this.offAll = offAll;
		this.off    = off;
		this.emit   = emit;
		this.once   = once;

		
		function once(eventID, callback) {
			var id = on(eventID, function (data) {
				off(eventID, id);
				callback(data);			
			});
		}
		
		function on(eventID, callback) {
			if (
				!callback
				|| !callback instanceof Function
			)
				return;
			
			if (typeof events[eventID] === 'undefined')
				events[eventID] = {
					actions : {},
					id      : 0
				};
			
			var event = events[eventID];
			event.actions[event.id++] = callback;

			return event.id;
		}
		
		
		function onAll(callback) {
			return on('*', callback);
		}
		
		function offAll(id) {
			return off('*', id);
		}

		function off(eventName, id) {
			if (typeof events[eventName] === 'undefined')
				return;
			
			delete events[eventName].actions[id];
		}
		
		function emit(eventName, eventData) {
			emitEvent.call(this, eventName, eventName, eventData);
			emitEvent.call(this, '*', eventName, eventData);
		}
		
		function emitEvent(eventID, eventName, eventData) {
			if (typeof events[eventID] === 'undefined')
				return;

			for (var id in events[eventID].actions)
				events[eventID].actions[id].call(this, eventData, eventName);
		}
	}

	exports.EventEmitter = EventEmitter;
})(typeof exports === 'undefined' ? this : exports);
