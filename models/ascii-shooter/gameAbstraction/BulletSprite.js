var EventEmitter = require (__dirname + '/../common/EventEmitter').EventEmitter;

module.exports = function BulletSprite(ID, XY, direction, world) {	
	/* extend */
	EventEmitter.call(this);
	
	/* construct */	
	var that = this;
	
	world.emit('bulletFired', getState());
	tryMoveTo(XY.x, XY.y);
	
	/* getters */
	this.getState = getState;
	this.getType  = function getID() { return "bullet" };

	this.advance = function advance() {
		switch (direction) {
			case 'left':
				moveLeft();
				break;
			case 'right':
				moveRight();
				break;
			case 'up':
				moveUp();
				break;
			case 'down':
				moveDown();
				break;
		}
	};

	function moveUp()    { tryMoveTo(XY.x    , XY.y - 1) };
	function moveLeft()  { tryMoveTo(XY.x - 1, XY.y    ) };
	function moveRight() { tryMoveTo(XY.x + 1, XY.y    ) };
	function moveDown()  { tryMoveTo(XY.x    , XY.y + 1) };

	function tryMoveTo(x, y) {		
		var _XY = {
			x: x,
			y: y
		};

		var collision = false,
				at;

		if ( x < 0 )
			collision = true;
		else if ( y < 0 )
			collision = true;
		else if ( x > world.map.getWidth() - 1)
			collision = true;
		else if ( y > world.map.getHeight() - 1)
			collision = true;
		else {
			at = world.map.getAt(x, y);
			if (at && at.ref.getType() !== 'bullet') {
				collision = true;
			}
		}

		if (collision) {
			destroy();

			if (at && at.ref.getType() === 'flesh') {
				at.ref.suffer();
			}

			return;
		}

		XY = _XY;
		move();
	}

	function move() {
		world.map.startDrawing('Bullet' + ID);
		world.map.draw(XY.x, XY.y, that);
		world.map.endDrawing();

		world.emit('bulletMove', getState());
	}

	function getState() {
		return {
			id : ID,
			x  : XY.x,
			y  : XY.y
		};
	}

	function destroy() {
		world.map.startDrawing('Bullet' + ID);
		world.map.endDrawing();

		that.emit('bulletGone', getState());
		world.emit('bulletGone', getState());
	}
};
