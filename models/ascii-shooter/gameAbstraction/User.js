var EventEmitter = require(__dirname + '/../common/EventEmitter').EventEmitter;

var Room         = require(__dirname  + '/Room');
var UserCommands = require(__dirname + '/UserCommands');

module.exports  = function User(userID, hall) {
	/* extend */
	EventEmitter.call(this);

	/* construct */
	var that = this;

	var nick;
	var _inGameId;

	var commands = new UserCommands(that);

	var room;
	var roomHandlers;

	/* define */

	/* getters/setters */
	this.getUserID = function getUserID() { return userID };
	this.getRoom   = function getRoom() { return room };
	this.getHall   = function getRoom() { return hall };
	this.getGame   = function getGame() {
		var room = this.getRoom(); if (!room) return;
		return room.getGame();
	};

	//this.setRoomHandlers = function setRoomHandlers(value) { roomHandlers = value };

	this.nickname  = function nickname(value) { if (value) nick = value; else return nick };
	this.inGameId  = function inGameId(value) { if (typeof value === 'undefined') return _inGameId; else _inGameId = value };


	this.loggedIn  = function loggedIn() { return Boolean(nick) };

	/* user methods */
	this.init = function init() {
		this.emitSelf('requireLogin', "What is your nickname?");
	};
	
	this.handleRoom = function handleRoom(handler, data) {
		if (!room || !roomHandlers || !roomHandlers[handler]) return null;
		
		return roomHandlers[handler](data);
	}
	
	
	this.createRoom = function createRoom() {
		if (this.getRoom())
			return false;

		var room = new Room(true);
		this.joinRoom(room);
		
		var hall = this.getHall();
		hall.enlistRoom(room);
		
		return true;
	};

	this.joinRoom  = function joinRoom(_room) {
		if (room)
			return false;

		room         = _room;
		roomHandlers = room.join(this);
		
		return true;
	};

	this.leaveRoom = function leaveRoom() {
		if (!room)
			return;

		var result = this.handleRoom('leave');
		if (room.getName() === null)
			hall.removeRoom(room);
		else
			hall.enlistRoom(room);

		room         = null;
		roomHandlers = null;
		
		return result;
	};	


	this.disconnect = function disconnect() {
		var game = this.getGame();
		var id   = this.inGameId();

		if (game)
			game.removeHero(this.inGameId());
		
		hall.removeUser(that.getUserID());
		this.leaveRoom();		
	};

	this.play = function play(action, data) {
		var game = this.getGame(); if (!game) return;		
		if (!game[action] || typeof this.inGameId() === 'undefined')
			return;

		game[action](this.inGameId(), data);
	};

	this.command = commands.command;

	this.emitSelf    = function emitSelf(eventName, payload) {
		var data = {
			eventName : eventName,
			payload   : payload
		};

		this.emit('emitSelf', data);
	}

	this.outputTo = function outputTo(text) {
		that.emitSelf('output', text + '\n');
	};


	this.login = function login(nickname) {
		if (this.loggedIn())
			return;
		
		var nickname = nickname.replace(/\W+/g, "").toLowerCase();
		
		if (!nickname)
			return;
		
		var usersByNickname = hall.getUsersByNickName();

		var i = 0;
		var nextNickName = nickname;

		while (usersByNickname[nextNickName]) {
			nextNickName = nickname + i;
			++i;
		}

		this.nickname(nextNickName);
		usersByNickname[nextNickName] = this;

		this.emitSelf('logged', nextNickName);
	};
};
