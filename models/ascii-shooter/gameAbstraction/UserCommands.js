module.exports = function UserCommands(user) {
	var that = this;
	
	var commands = {};

	this.command = function command(line) {
		if ( !user.loggedIn() )
			return;
		
		var params = line.toLowerCase().replace(/\s+/g, ' ').split(' ');
		var name   = params[0];

		if (!commands[name]) {
			user.outputTo('Unknown command "' + name + '". Type "help" to see commands.');
			return;
		}
		
		params.shift();
		user.outputTo(commands[name].exec.apply(null, params));
	};

	commands.help = new Command(
		'help',
		'Lists commands and help',
		'[command]',
		function help(commandName) {
			var text = '';
			if (commandName) {
				text = commands[commandName]
					? commands[commandName].getFullInfo()
					: 'There is no command named "' + commandName + '"';
			}
			else {
				text = 'Commands:\n';

				for (var name in commands)	text += commands[name].getFullInfo();				
			}
			
			return text;
		}
	);
	
	
	commands.room = new Command(
		'room',
		'Opens room',
		'',
		function room() {
			if (user.createRoom())
				return 'Room Opened!';
			else
				return 'Leave the room you are in to create new room.';
		}
	);
	
	commands.ls = new Command(
		'ls',
		'Lists rooms',
		'',
		function ls() {
			var hall = user.getHall();
			return hall.listRooms();
		}
	);
	
	commands.join = new Command(
		'join',
		'Joins room',
		'name',
		function join(name) {			
			var hall = user.getHall();
			var room = hall.getRoom(name);
			
			if (!room)
				return 'Room does not exist.';
			
			if (user.joinRoom(room))
				return 'Joined room!';
			else
				return 'You joined the "' + user.getRoom().getName() +'" room already!\n'
								+ 'Please wait for room admin to start game.';
		}
	);
	
	
	commands.leave = new Command(
		'leave',
		'leaves current room',
		'',
		function join(name) {
			if (user.leaveRoom())
				return 'Room left!';
			else
				return 'No room to leave.';
		}
	);
	
	commands.start = new Command(
		'start',
		'Starts game in current room',
		'',
		function start() {			
			var room = user.getRoom();
			
			if (!room)
				return 'Create room firstly!';
			
			if (user.handleRoom('createGame') === null)
				return 'You have to be admin of the room to start game!';
			
			user.emit('createGame');
			user.handleRoom('startGame');
		}
	);
	
	
	/* Command class */
	function Command(name, description, usage, func) {
		var that = this;
		
		
		this.getName        = function getName()        { return name };
		this.getDescription = function getDescription() { return description };
		this.getUsage       = function getUsage()       { return usage };
		
		this.getFullInfo    = function getFullInfo() {
			var text = that.getName() + ': ' + that.getDescription() + '\n'
								+ '  usage: ' + that.getName() + ' ' + that.getUsage() + '\n';

			return text;		
		};	

		
		this.exec = func;
	}
};
