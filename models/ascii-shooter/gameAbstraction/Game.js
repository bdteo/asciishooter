//todo: use XY object for direction
var ci = require('correcting-interval');

var EventEmitter = require (__dirname + '/../common/EventEmitter').EventEmitter;

var Map           = require(__dirname + '/Map');
var HeroFactory   = require(__dirname + '/HeroFactory');
var BulletFactory = require(__dirname + '/BulletFactory');

var mapFile;

/**
 * @throws if _mapFile argument not valid (width, height)
 */
var Game = function Game(_mapFile) {
	//console.log(arguments.callee.caller.toString());

	if (!_mapFile || isNaN(_mapFile.length) || isNaN(_mapFile[0].length))
		throw 'Valid mapFile needed';
	
	/* extend */
	EventEmitter.call(this);

	/* construct */
	mapFile = _mapFile;
	
	this.heroes       = {};
	this.heroesCount  = 0;
	this.bullets      = {};
	this.bulletsCount = 0;
	
	this.map = new Map(mapFile);
	this.heroFactory   = new HeroFactory(this);
	this.bulletFactory = new BulletFactory(this);
	
	gameLoop(this);
};



/* define */

/* getters */
Game.prototype.getMapFile  = function getMapFile() { return mapFile };

Game.prototype.getWidth  = function getWidth() { return mapFile[0].length };
Game.prototype.getHeight = function getHeight() { return mapFile.length };

Game.prototype.getHero   = function getHero(id) {
	if (!this.heroes[id])
		return;
	return this.heroes[id].getState();
};


/* add and remove hero methods*/

Game.prototype.addHero    = function addHero(nickname) {
	var id = this.getNextHeroID();
	if (id !== 0 && !id)
		return;
		
	var hero = this.heroes[id] = this.heroFactory.create(id, nickname);
	++this.heroesCount;

	return id;
};

Game.prototype.removeHero = function removeHero(id) {
	if (!this.heroes[id])
		return;

	if (this.heroes[id].die()) {
		delete this.heroes[id];
		--this.heroesCount;
		this.emit('heroLeft', { id: id });
		//console.log(this.heroes);
	}
};



Game.prototype.getNextHeroID = function getNextHeroID() {
	if (this.heroesCount >= 3)
		return null;
	
	var id;

	for (var i = 0;;++i) {
		if (this.heroes[i] === undefined) {
			id = i;
			break;
		}
	}
	
	return id;
}



/* hero methods*/
Game.prototype.up    = function up(id)    { this.heroes[id].setDirectionY('up') };
Game.prototype.down  = function down(id)  { this.heroes[id].setDirectionY('down') };
Game.prototype.left  = function left(id)  { this.heroes[id].setDirectionX('left') };
Game.prototype.right = function right(id) { this.heroes[id].setDirectionX('right') };

Game.prototype.stopX = function stopX(id) { this.heroes[id].setDirectionX(null) };
Game.prototype.stopY = function stopY(id) { this.heroes[id].setDirectionY(null) };

Game.prototype.shoot = function shoot(id, direction) { this.heroes[id].shoot(direction) };
Game.prototype.chat  = function chat(id, text)  { this.emit('chatMsg', { id: id, text: text}) };

Game.prototype.spawn = function spawn(id) { this.heroes[id].spawn() };

/* gameLoop */
function gameLoop(world) {
	var heroesAdvanceInterval = ci.setCorrectingInterval(function () {
		for (var id in world.heroes) {
			world.heroes[id].advance();
		}
	}, Math.floor(1000/35));
	
	var bulletsAdvanceInterval = ci.setCorrectingInterval(function () {
		for (var id in world.bullets) {
			world.bullets[id].advance();
		}
	}, Math.floor(1000/50));
}

module.exports = Game;
