var HeroSprite = require (__dirname + '/HeroSprite.js');

module.exports = function HeroFactory(world) {
	/* construct */

	var rawHeroTemplate = [
			' @ ',
			'@*@',
			' @ '
	];

	/* define */

	this.create = function create(id, nickname) {
		var XY     = getSpawnXY(id);
		var sprite = new HeroSprite(id, nickname, XY, rawHeroTemplate, world);
		
		attachSpriteEvents(sprite);		
		
		return sprite;
	};
	
	function attachSpriteEvents(sprite) {
		sprite.on('born', function born() {
			var data = {
				id       : this.getID(),
				nickname : this.getNickname(),
				XY       : this.getXY(),
				health   : this.getHealth(),
				deaths   : this.getDeaths()
			};

			world.emit('heroBorn', data);
		});

		
		sprite.on('move', function move() {
			var data = {
				id : this.getID(),
				XY : this.getXY()
			};

			world.emit('heroMove', data);
		});
		
		sprite.on('healthChange', function healthChange() {
			var data = {
				id     : this.getID(),
				health : this.getHealth()
			};

			world.emit('heroHealthChange', data);
		});
		
		
		sprite.on('dead', function () {
			var data = {
				id       : this.getID(),
				nickname : this.getNickname(),
				XY       : this.getXY(),
				health   : this.getHealth(),
				deaths   : this.getDeaths()
			};

			world.emit('heroDead', data);
		});
	}

	function getSpawnXY(id) {
		var XY = {
			x: null,
			y: null
		};

		switch (id) {
			case 0:
				XY.x = 2;
				XY.y = 2;
				break;
			case 1:
				XY.x = world.map.getWidth() - 3;
				XY.y = 2;
				break;
			case 2:
				XY.x = world.map.getWidth() - 3;
				XY.y = world.map.getHeight() - 3;
				break;
			case 3:
				XY.x = 2;
				XY.y = world.map.getHeight() - 3;
				break;
			default:
				XY.x = Math.floor(world.map.getWidth()/2);
				XY.y = Math.floor(world.map.getHeight()/2);
		}

		return XY;
	}
};
