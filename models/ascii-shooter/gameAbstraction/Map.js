module.exports = function Map(mapFile) {
	/* construct */
	var that = this;
	
	var map;
	var lastUsedXYs  = {};
	var currentGroup = null;
	
	var width  = mapFile[0].length;
	var height = mapFile.length;
	
	initializeMap();
	drawWalls();

	/* getter methods */
	this.getWidth  = function getWidth()  { return width };
	this.getHeight = function getHeight() { return height };
	this.getAt     = getAt;
	
	function getAt(x, y) { if (map[y] === undefined) return; return map[y][x]};

	/* drawing methods */
	/**
	 * @throws if group id is not specified
	 */
	this.startDrawing = startDrawing;
	
	function startDrawing(group) {
		if (!group) throw 'Group id not specified!';

		currentGroup = group;
		clearCurrentGroup();
	};

	this.endDrawing = endDrawing;

	function endDrawing() {
		currentGroup = null;
	}

	/**
	 * @throws if group id is not specified
	 */
	this.draw = draw;

	function draw(x, y, ref) {
		if (!currentGroup) throw 'Group id not specified!';

		if (map[y] === undefined)
			return;
		if (map[y][x] === undefined)
			return;

		map[y][x] = ref ? { group : currentGroup, ref : ref } : null;

		lastUsedXYs[currentGroup].push({
			x: x,
			y: y
		});
		
		return true;
	}

	/* helper functions */
	function clearCurrentGroup() {
		if (!lastUsedXYs[currentGroup]) {
			lastUsedXYs[currentGroup] = [];
			return;
		}
		
		clearXYs(lastUsedXYs[currentGroup]);
		lastUsedXYs[currentGroup] = [];
	}


	function clearXYs(XYs) {
		var at;

		for (var i = 0, il = XYs.length; i<il; ++i) {
			at = getAt(XYs[i].x, XYs[i].y);

			if (!at || at.group === currentGroup)
				draw(XYs[i].x, XYs[i].y, null);
		}
	}

	function initializeMap() {
		map = [];

		for (var i = 0; i<height; ++i) {
			var row  = [];

			for (var j = 0; j<width; ++j) {				
				row.push(null);
			}

			map.push(row);
		}
	}

	function drawWalls() {
		startDrawing('walls');
		
		for (var i = 0; i<height; ++i) {
			var line = mapFile[i];

			for (var j = 0; j<width; ++j) {
				var char = line.charAt(j);
				if (char !== ' ')
					draw(j, i, {
						getType : function getType() { return "wall" }
					});
			}
		}
		
		endDrawing();
	}	
	
};
