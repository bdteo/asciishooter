var EventEmitter = require(__dirname + '/../common/EventEmitter').EventEmitter;
var Game    = require(__dirname  + '/Game');
var mapFile = require(__dirname  + '/mapFile');

module.exports = function Room(autoApprove) {
	/* extend */
	EventEmitter.call(this);

	/* construct */
	var that = this;

	var waiting       = {},
			approved      = {},
			approvedCount = 0,
			maxApproved   = 4;
			
	var admin;
	var previousAdminName;

	var game;

	/* define*/
	this.join            = join;
	this.getName         = function getName() { return admin ? admin.user.nickname() : null };
	this.getPreviousName = function getPreviousName() { return previousAdminName };
	this.getGame         = function getGame() { return game };
	
	this.getApprovedNames = function getApprovedNames() {
		var result = [];

		for (var id in approved) {
			var user = approved[id].user;
			result.push(user.nickname());
		}
		
		return result;
	};

	function join(user) {
		var id = user.getUserID();
		waiting[id] = {
			user        : user,
			approvalID  : null
		};
		
		
		if (approvedCount) {
			if (autoApprove)
				approve(id);
		} else {
			approve(id);//first one to join (the creator) is approved always//todo: set admin explicitly?
			updateAdmin();
		}
		
		toAll('user "' + user.nickname() + '" joined');

		return getRoomHandlers(id);
	}
	
	function isApproved(id) {
		return Boolean(approved[id]);
	}
	
	function isAdmin(userID) {
		if (!approved[userID])
			return false;
		
		return userID === admin.user.getUserID();
	}
	
	function updateAdmin() {	
		var adminNameBefore = that.getName();
			
		var newAdmin;
		for (var id in approved) {
			if (typeof newAdmin === 'undefined')	newAdmin = approved[id];
			else if (newAdmin.approvalID > approved[id].approvalID)	newAdmin = approved[id];
		}
		
		admin = newAdmin;//todo - new handlers for new admin

		var adminNameAfter = that.getName();
		if (adminNameBefore !== adminNameAfter)			
			previousAdminName = adminNameBefore;
	}


	function approve(userID) {
		if (!waiting[userID] || approvedCount === maxApproved)
			return;
		
		var user = waiting[userID];
		delete waiting[userID];
		
		user.approvalID = approvedCount;
		approved[userID] = user;
		++approvedCount;	
	}

	function kick(userID) {
		leave(userID);
	}

	function leave(userID) {
		var user;
		if (approved[userID]) {
			user = approved[userID];

			delete approved[userID];
			--approvedCount;
			
			updateAdmin();	
			
			forApproved(function rejoin(mate) {
				mate.user.emit('join', that.getName());
			});
		}
		
		
		if (waiting[userID]) {
			user = approved[userID];

			delete waiting[userID];
		}
		
		if (user) {
			toAll('user "' + user.user.nickname() + '" left');
			return true;
		}
		
		return false;
	}
	
	function createGame() {		
		game = new Game(mapFile);
	}

	function startGame() {
		if (!game)
			return;

		for (var id in approved) {
			var user   = approved[id].user;
			var gameId = game.addHero(user.nickname());//todo: Exception??

			user.inGameId(gameId);

			user.emit('join', that.getName());
			user.emitSelf('gameStarted', {
				mapFile : game.getMapFile()
			});			
		}
		
		for (var id in approved) {
			var user   = approved[id].user;
			var gameId = user.inGameId();

			game.spawn(gameId);
		}
	}
	

	
	function getRoomHandlers(id) {
		var _leave      = function () { return leave(id) };
		var _isApproved = function () { return isApproved(id) };
		
		var adminHandlers = {
			approve    : approve,
			kick       : kick,
			leave      : _leave,
			startGame  : startGame,
			createGame : createGame
		};
		
		var regularHandlers = {
			leave      : _leave,
			isApproved : _isApproved
		};
		
		return isAdmin(id)
			? adminHandlers 
			:	regularHandlers;
	}
	
	function forAll(callback) {
		forWaiting(callback);
		forApproved(callback);
	}
	
	function forWaiting(callback) {
		for (var id in waiting)  callback(waiting[id]);
	}
	
	function forApproved(callback) {
		for (var id in approved)  callback(approved[id]);
	}	
	
	function toAll(message) {
		forAll(function (mate) {
			mate.user.outputTo(message);
		});
	}
};
