var EventEmitter = require(__dirname + '/../common/EventEmitter').EventEmitter;

var HeroSprite = function HeroSprite(ID, nickname, spawnXY, template, world) {
	/* extend */
	EventEmitter.call(this);
	
	/* construct */	
	var that = this;


	var heroTemplate = compileTemplate(template),
			halfLen      = Math.floor(template.length/2);
	
	var XY;
	var directionX,
			directionY;
	var nonNullDirection;
	
	var health,
			advancesBeforeRespawn;
	
	var shots;
	
	var deaths = 0;
		
	/* define */

	/* getter methods */
	this.getID       = function getID() { return ID };
	this.getNickname = function getNickname() { return nickname };

	this.getType     = function getType()    { return "flesh" };
	
	this.getHealth   = function getHealth()  { return health };
	this.getIsAlive  = function getIsAlive() { return this.getHealth() > 0 };
	this.getXY       = function getXY()      { return XY };
	this.getDeaths   = function getDeaths()  { return deaths };
	
	this.getState  = function () {
		return {
			XY     : XY,
			health : health,
			deaths : deaths
		}
	};
	
	/* spawn and die methods */
	this.spawn = function spawn() {
		XY               = spawnXY;
		directionX       = null;
		directionY       = null;
		nonNullDirection = 'right';
		
		
		health           = 9;
		shots            = [];
		
		this.emit('born', this.getState());
		move();
	}
	
	this.die   = function die() {
		++deaths;
		advancesBeforeRespawn = 200;

		world.map.startDrawing('hero' + ID);
		world.map.endDrawing();

		that.emit('dead', this.getDeaths());
		return true;
	};
	
	/* actions */	
		/* suffer method */
	this.suffer = function suffer() {
		liveWrap(function () {
			--health;
			this.emit('healthChange', this.getHealth());
		
			if (!health)
				this.die();
		});
	};
	

	/* direct methods */
	this.setDirectionX = function setDirectionX(dir) {
		return liveWrap(function () {
			directionX = dir;
			if (dir)
				nonNullDirection = dir;
		});
	};

	this.setDirectionY = function setDirectionY(dir) {
		return liveWrap(function () {
			directionY = dir;
			if (dir)
				nonNullDirection = dir;
		});
	};

	/* advance method */
	this.advance = function advance() {
		if (that.getIsAlive()) {
			switch (directionX) {
				case 'left':
					moveLeft();
					break;
				case 'right':
					moveRight();
					break;
			}

			switch (directionY) {
				case 'up':
					moveUp();
					break;
				case 'down':
					moveDown();
					break;
			}
			
			if (shots.length)
				shots.pop()();
		}
		else {
			if (advancesBeforeRespawn)
				--advancesBeforeRespawn;
			else
				this.spawn();
		}
	}

	this.shoot = function shoot(direction) {
		return liveWrap(function () {
			shots.push(function () {
				var ID     = world.bulletsCount;
				var bullet = world.bulletFactory.create(ID, XY, halfLen, direction);//nonNullDirection);

				if (bullet) {
					world.bullets[ID] = bullet;
					++world.bulletsCount;

					bullet.on('bulletGone', function bulletGone(state) { delete world.bullets[ID]	});
				}
			});
		});
	};
	
	
	function liveWrap(action) {
		if (!that.getIsAlive())
			return;
		return action.apply(that);
	}

	/*constructor helper*/

	function compileTemplate(template) {
		var heroTemplate = [];

		for (var y = 0, yl = template.length; y<yl; ++y) {
			var line = template[y].split('');

			for (var x = 0, xl = line.length; x<xl; ++x) {
				var chr = line[x];
				if (chr !== ' ')
					heroTemplate.push({
						x : x,
						y : y
					});
			}
		}
		
		return heroTemplate;		
	};

	/* move helpers */
	function moveUp()    { tryMoveTo(XY.x    , XY.y - 1) };
	function moveLeft()  { tryMoveTo(XY.x - 1, XY.y    ) };
	function moveRight() { tryMoveTo(XY.x + 1, XY.y    ) };
	function moveDown()  { tryMoveTo(XY.x    , XY.y + 1) };

	function tryMoveTo(x, y) {
		var _XY = {
			x: x,
			y: y
		};

		if ( x < 0 ) return;
		if ( y < 0 ) return;
		if ( x > world.map.getWidth() - 1) return;
		if ( y > world.map.getHeight() - 1) return;


		var collision = false,
				at;

		var oldSpread = {};
		spreadAt(XY, function (x, y) {
			if (!oldSpread[x])
				oldSpread[x]  = {};
			oldSpread[x][y] = 1;
		});


		spreadAt(_XY, function (x, y) {
			if (oldSpread[x] && oldSpread[x][y])
				return;

			at = world.map.getAt(x, y);

			if (at && at.ref.getType() !== 'bullet') {
				collision = true;
				return false;
			}
		});

		if (collision) return;

		XY = _XY;
		move();
	}

	function move() {
		world.map.startDrawing('hero' + ID);
		spreadAt(XY, function (x, y) {
			world.map.draw(x, y, that);
		});
		world.map.endDrawing();

		that.emit('move', that.getXY());
	}

	function spreadAt(XY, callback) {
		var H = heroTemplate,
				x, y;

		for (var j = 0, jl = H.length; j<jl; ++j) {
			x = XY.x + H[j].x - halfLen;
			y = XY.y + H[j].y - halfLen;

			if (callback(x, y) === false)
				return;
		}
	}	
};


module.exports = HeroSprite;
