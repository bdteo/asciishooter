var BulletSprite = require(__dirname + '/BulletSprite');

module.exports = function BulletFactory(world) {
	/* construct */

	/* define */

	this.create = function create(ID, parentXY, parentWidth, direction) {
		if (!direction)
			return;

		var spawnXY = getSpawnXY(direction, parentXY, parentWidth);
		if (spawnXY.x === null)
			return;

		var sprite  = new BulletSprite(ID, spawnXY, direction, world);

		//attachSpriteEvents(sprite);
		//sprite.spawn();
		
		return sprite;
	};	
	
	function getSpawnXY(direction, parentXY, parentWidth) {
		var XY  = parentXY;
		var res = {x: null, y: null};

		switch (direction) {
			case 'left':
				res.x = XY.x - parentWidth - 1;
				res.y = XY.y;
				break;
			case 'right':
				res.x = XY.x + parentWidth + 1;
				res.y = XY.y;
				break;
			case 'up':
				res.x = XY.x;
				res.y = XY.y - parentWidth - 1;
				break;
			case 'down':
				res.x = XY.x;
				res.y = XY.y + parentWidth + 1;
				break;
		}
		
		return res;
	}
}
