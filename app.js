var fs = require('fs');

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var port = process.env.PORT || '3000';

var app = express();
app.set(port);

var server = app.httpServerInstance = app.listen(port);
var io     = app._io                = require('socket.io').listen(server);

require('./models/ascii-shooter/modelIO/ModelServer')(app);


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hjs');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
//##app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


//load controllers
(function () {
	var controllersDir = './controllers',
			baseDirRegex   = new RegExp('^' + controllersDir);

	walk(controllersDir, function (name, dir) {
		if(name.substr(-3) === '.js') {
			var route  = dir.replace(baseDirRegex, ''),
					path   = dir + '/' + name,
					router = require(path);

			app.use(route, router);
		}
	});
})();

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = app;


function walk(dir, callback) {
	var path,
			list = fs.readdirSync(dir);

	list.forEach(function (file) {
		path = dir + '/' + file;
		var stat = fs.statSync(path);
		if (stat && stat.isDirectory()) walk(path, callback);
		else callback(file, dir);
	});
}
